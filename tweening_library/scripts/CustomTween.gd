extends Node


class_name CustomTween


#func _init(start_v:float, end_v:float, duration:float):
#	self.start_v = start_v
#	self.end_v = end_v
#	self.duration = duration
#	self.t = start_v

var start:Vector2
var end: Vector2
var duration: float
var t_segment:float
var t:float
var ease_type
var _on_end:Callable
var targets: Array[Vector2]
var num_of_targets : int = 0

var object:Node2D
var property:StringName
#
func _init(object, property, start, end, duration:float, ease_type, function: Callable):
	self.start = start
	self.end = end
	self.duration = duration
	self.ease_type = ease_type
	self._on_end = function
	self.num_of_targets +=1

	self.object =object
	self.property = property
	
	self.t = 0
	self.t_segment = 0
	

func update(delta:float):
	var duration_segment = self.duration / (self.num_of_targets)
	var weight = self.t_segment / duration_segment
	
	if self.ease_type != null:
		weight = EaseType.interp(self.ease_type, weight) 
	
	var value = lerp(self.start,self.end,weight)
	
	self.object.set(self.property,value)
	
	self.t += delta
	self.t_segment += delta
	
	self.t_segment = duration_segment if self.t_segment >= duration_segment else self.t_segment
	self.t = self.duration if self.t >= self.duration else self.t
	
	if is_done_segment(duration_segment):
		var next = self.targets.pop_front()
		if next != null:
			print(self.t)
			self.t_segment = 0
			self.start = self.object.get(self.property) 
			self.end = next
			
	

	
func then_to(target:Vector2) ->CustomTween:
	print("then to called on vector", str(target))
	self.targets.append(target)
	self.num_of_targets +=1
	return self


func is_done_segment(duration_local:float) -> bool:
	# Task 1 - implement this
	if self.t_segment >= duration_local:
		print("segment done")
	return self.t_segment >= duration_local
		
func is_done() -> bool:
	# Task 1 - implement this
	return self.t >= self.duration
