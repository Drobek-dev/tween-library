extends Node


enum {
	NONE,
	EASE_IN_QUAD,
	EASE_OUT_QUAD,
	EASE_IN_OUT_QUAD,
	SMOOTHSTEP
}

static func interp(ease_type,t):
	match ease_type:
		EASE_IN_QUAD:
			return ease_in_quad(t)
		EASE_OUT_QUAD:
			return ease_out_quad(t)
		EASE_IN_OUT_QUAD:
			return ease_in_out_quad(t)
		SMOOTHSTEP:
			return smoothstep_(t)
	return t
	
static func ease_in_quad(t):
	return t*t

static func ease_out_quad(x):
	return 1 - (1 - x) * (1 - x)
	
static func ease_in_out_quad(x):
	if x < 0.5:
		return 2 * x * x
	else:
		return 1 - pow(-2 * x + 2, 2) / 2
		
static func smoothstep_(t):
	return t*t*(3-2*t)
		

